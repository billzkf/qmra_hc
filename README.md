# QMRA Health Canada #

## What is this repository for? ##

* Quantitative Microbial Risk Assessment Suite for Health Canada
* 0.0.1

## How do I get set up? ##

1. Install R.
2. Install node.js.
3. Clone this repository.
4. Run ```npm install``` to get node dependencies (see below)
5. Configure your R paths (working directory and libraries directory) in config.r and main.r.
6. Configure and start RServe in daemon mode - by running ```RScript R/RServeStart.r```.
7. Configure node server (optional : defaults to port 3000).
8. To test app config, run : ```node .```.
9. Once app is installed, tested, and ready : ```forever .```.
10. Go to http://localhost/main, to load the app.

### R libraries
* RServe
* RJSONIO

### Node Dependencies
*These should all install if you ```npm install```. For reference:*

* Strongloop (express based node http server)
* forever (run node applications)
* Rio (node to Rserve)
* Vue.js (client-side templating and ui library)
* plotly.js (extensive plotting library)
* jStat (javascript statistical library)


## How to run  tests :
* R Tests (by RUnit) can be run from URL extension "/rtests" or from command line, using "Rscript R/TestRunner.r".
* JavaScript tests can be run by installing karma test runner, and typing ```karma start```.