describe('treatments', function() {
  /*
  * Test javascript NCSTR code against excel results
  */
  var tst = {
    hrt: [45, 60, 90, 120],
    BF: [0.1, 0.5, 0.35, 0.75],
    initConc: [0.5, 1.0, 0.65, 0.8],
    kValue: [0.001, 0.002, 0.003, 0.004],
    temp: [5, 10, 15, 20],
    pH: [5, 6, 7, 8]
  };

  it('should calculate Free Chlorine log removal', function() {
    var xl = [{
      crypto: 0.0063523161,
      giardia: 0.5506438524,
      rota: 1.4995599017,
      campy: 3.1765876429,
      eColi: 4.2293095549
    }, {
      crypto: 0.0159258393,
      giardia: 1.6321430540,
      rota: 8,
      campy: 8,
      eColi: 8
    }, {
      crypto: 0.0140284746,
      giardia: 1.2554748960,
      rota: 7.1478634301,
      campy: 7.6135908057,
      eColi: 8
    }, {
      crypto: 0.0214368479,
      giardia: 2.7585541578,
      rota: 8,
      campy: 8,
      eColi: 8
    }];

    for (var i = 0; i < tst.hrt.length; i++) {
      var res = getReducedConc('freeChlorine', tst.hrt[i], tst.BF[i], tst.temp[i], tst.initConc[i], tst.kValue[i], tst.pH[i]);
      for (var org in res.reduction) {
        expect(res.reduction[org]).toBeCloseTo(xl[i][org], 4);
      }
    }
  });

  it('should calculate Chloramine log removal', function() {
    var xl = [{
      crypto: 0.0004400934,
      giardia: 0.0296512111,
      rota: 0.1822099964,
      campy: 1.1190526633,
      eColi: 0.4064946555
    }, {
      crypto: 0.0004684354,
      giardia: 0.0849664262,
      rota: 0.6850034781,
      campy: 4.7135701826,
      eColi: 1.3397472166
    }, {
      crypto: 0.0004511227,
      giardia: 0.0960539162,
      rota: 0.6962885683,
      campy: 3.0810245908,
      eColi: 1.0532586953
    }, {
      crypto: 0.0004621584,
      giardia: 0.2118455364,
      rota: 1.8577383695,
      campy: 8.0000000000,
      eColi: 2.2147274203
    }];

    for (var i = 0; i < tst.hrt.length; i++) {
      var res = getReducedConc('chloramine', tst.hrt[i], tst.BF[i], tst.temp[i], tst.initConc[i], tst.kValue[i], tst.pH[i]);
      for (var org in res.reduction) {
        expect(res.reduction[org]).toBeCloseTo(xl[i][org], 4);
      }
    }
  });

  it('should calculate OZone log removal', function() {
    var xl = [{
      crypto: 0.6136248056,
      giardia: 2.0736868819,
      rota: 2.3233896034,
      campy: 8,
      eColi: 8
    }, {
      crypto: 2.9912041576,
      giardia: 4,
      rota: 4,
      campy: 8,
      eColi: 8
    }, {
      crypto: 2.5950393228,
      giardia: 4,
      rota: 4,
      campy: 8,
      eColi: 8
    }, {
      crypto: 6,
      giardia: 4,
      rota: 4,
      campy: 8,
      eColi: 8
    }];

    for (var i = 0; i < tst.hrt.length; i++) {
      var res = getReducedConc('ozone', tst.hrt[i], tst.BF[i], tst.temp[i], tst.initConc[i], tst.kValue[i], tst.pH[i]);
      for (var org in res.reduction) {
        expect(res.reduction[org]).toBeCloseTo(xl[i][org], 4);
      }
    }
  });

  it('should calculate Chlorine Dioxide log removal', function() {
    var xl = [{
      crypto: 0.0478353938,
      giardia: 0.8655888276,
      rota: 2.3695887922,
      campy: 2.8175871908,
      eColi: 2.8175871908
    }, {
      crypto: 0.1948711995,
      giardia: 3.4535547876,
      rota: 8,
      campy: 4,
      eColi: 4
    }, {
      crypto: 0.2535568643,
      giardia: 2.3917081003,
      rota: 6.3627964619,
      campy: 4,
      eColi: 4
    }, {
      crypto: 0.6410073484,
      giardia: 4,
      rota: 8,
      campy: 4,
      eColi: 4
    }];

    for (var i = 0; i < tst.hrt.length; i++) {
      var res = getReducedConc('chlorineDioxide', tst.hrt[i], tst.BF[i], tst.temp[i], tst.initConc[i], tst.kValue[i], tst.pH[i]);
      for (var org in res.reduction) {
        expect(res.reduction[org]).toBeCloseTo(xl[i][org], 4);
      }
    }
  });

})
