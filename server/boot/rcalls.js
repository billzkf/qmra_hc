var rio = require('rio');
var path = require('path');
module.exports = function(app) {
  app.get('/connecttest', function(req, res){
      console.log('reached node')
      rio.$e({
        command: 'getwd()'
      })
      .then(function(rRes, any){
        res.send({testResult:rRes});
      })
      .catch(function(err){
        console.log(err);
      })
  })

  app.get('/rtests', function(req, res) {

    rio.$e({
        filename: path.join(process.cwd(), '/R/TestRunner.r'),
        entrypoint: "runTests"
      })
      .then(function(rRes) {
        res.redirect('/results.html');
      });
  });

  app.post('/all', function(req, res) {
    rio.$e({
        filename: path.join(process.cwd(), '/R/main.r'),
        entrypoint: "init",
        data: req.body
      })
      .then(function(Rres, any) {
        res.send({result: Rres});
      })
      .catch(function(err){
        console.log("Error: ",  err);
      });
  });
};
