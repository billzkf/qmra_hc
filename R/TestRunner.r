require(RUnit)
require(RJSONIO)
setwd("/Users/williamflynn/Dev/qmra_hc")
source('./R/main.r')

runTests <- function(){
  test.suite <- defineTestSuite("QMRA_HC", file.path("./R/tests"), testFileRegexp = '[[:print:]].r')
  test.result <- runTestSuite(test.suite)
  printTextProtocol(test.result)
  printHTMLProtocol(test.result, fileName="./client/results.html")
}
