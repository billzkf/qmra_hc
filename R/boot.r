### This file is loaded when an RServe daemon is instantiated.
### The working dir and library path needs to be configured
### for each installation.

basePath <- "/Users/williamflynn/Dev/qmra_hc"
setwd(basePath)
.libPaths("/Library/Frameworks/R.framework/Resources/library")

requiredPackages <- c("RJSONIO")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}
