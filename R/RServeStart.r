### This script runs when the application is initiated.
### It starts the Rserve instance
# configure the install by changing the options here.
libPath <- "/home/qmrawikiuser/R/x86_64-pc-linux-gnu-library/2.15/"
basePath <- "/home/qmrawikiuser/public_html/qmra_hc/"

.libPaths(libPath)
require(Rserve)
Rserve(args=paste("--RS-conf ",basePath,"R/Rserv.conf",sep=""))
