hrt <- 60
BF <- 0.5
time <- 0.1715942234
residual <- 0.9996568704
temp <- 10
pH <- 6
kValue <- 0.00200
initConc <- 1

test.BaffleNCSTR <- function(){
  checkEquals(5.4186625, BaffleNCSTR(BF))
}

test.ReactorChar <- function(){
  rcTestVals <- data.frame(
    xValue=c(0.0154968531),
    CDF=c(0),
    PDF=c(0),
    time=c(0.1715942234),
    residual=c(0.9996568704),
    IntCt=c(0.1715647822)
  )
  sumXvalue <- 7756.174955
  sumCDF <- 650.9230004
  sumPDF <- 0.999
  sumTime <- 85882.9088
  sumResid <- 846.3153789
  sumInitConc <- 76842.31054
  rc <- ReactorChar(hrt, BF, temp, pH, kValue, initConc)
  checkEqualsNumeric(rcTestVals[1,]$xValue, rc[2,]$xValue)
  checkEqualsNumeric(rcTestVals[1,]$residual, rc[2,]$residual)
  checkEquals(sumXvalue, sum(rc$xValue))
}

# passes but moved to client side javascript
# commented out to increase test speed
#test.LogInactivation <- function(){
  #liTestVals <- list(
    #freeChlorine=data.frame(
      #crypto=c(0.0000476487),
      #giardia=c(0.0069224765),
      #rota=c(0.4085107445),
      #campy=c(1.2487773067),
      #eColi=c(2.2462252893)
      #)
    #)
  #sumCrypto <- 21.34142378
  #sumGiardia <- 3151.319614
  #rc <- ReactorChar(hrt, BF, temp, pH, kValue, initConc)
  #li <- LogInactivation(rc, temp, pH)
  #checkEquals(liTestVals[["freeChlorine"]][["rota"]][1], li[["freeChlorine"]][["rota"]][2])
  #checkEquals(sum(li[["freeChlorine"]][["giardia"]]), sumGiardia)
  #checkEquals(sum(li[["freeChlorine"]][["crypto"]]), sumCrypto)
#}


#test.RemainingOrganisms <- function(){
  
  #rc <- ReactorChar(hrt, BF, temp, pH, kValue, initConc)
  #li <- LogInactivation(rc, temp, pH)
  #ro <- RemainingOrganisms(rc, li)
  #sumCrypto <- sum(ro[["freeChlorine"]][["crypto"]])
  #red <- -log10( sumCrypto / 1E50)
  #checkEquals(0.0159258393, red)
#}
