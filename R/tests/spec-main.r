

test.DoseResponse.crypto <- function(){
  crypto <- DoseResponse(10,10,4.29228,'Exponential',c(0.018))
  checkEquals(9.07E-08, round(sum(crypto$wPrOfInf),11))
}

test.DoseResponse.giardia <- function(){
  giardia <- DoseResponse(50,50,5.16289,'Exponential',c(0.01982))
  checkEquals(6.726E-08, round(sum(giardia$wPrOfInf),11))
}

test.DoseResponse.rota <- function(){
  rota <- DoseResponse(10,10,10.870,'BetaBinom',c(0.2650,0.4415))
  checkEquals(5.11E-13, sum(rota$wPrOfInf))
}

test.DoseResponse.campy <- function(){
  campy <- DoseResponse(10000,10000,10.420,'BetaBinom',c(0.145,7.5900))
  checkEquals(7.23E-11, sum(campy$wPrOfInf))
  
  campyNoTreat <- DoseResponse(10000,10000,0,'BetaBinom',c(0.145,7.5900))
  checkEquals(1.646E-01, round(sum(campyNoTreat$wPrOfInf), 4))
}

test.DoseResponse.eColi <- function(){
  eColi <- DoseResponse(3400,3400,10.420,'BetaBinom',c(0.05710, 2.21830))
  checkEquals(3.29E-11, sum(eColi$wPrOfInf))
  
  eColiNoTreat <- DoseResponse(3400,3400,0,'BetaBinom',c(0.05710, 2.21830))
  checkEquals(1.31E-1, round(sum(eColiNoTreat$wPrOfInf), 3))
}
