# prod path /home/qmrawikiuser/public_html/qmra_hc/R/boot.r
source("/Users/williamflynn/Dev/qmra_hc/R/boot.r")
# configure the install by changing the options here.

defaultSlice <- 0.002
frameSize <- (1 - defaultSlice) / defaultSlice
# Load the main application files
source(paste(basePath,"/R/qmra.r",sep=""))
source(paste(basePath,"/R/treatment.r",sep=""))
source(paste(basePath,"/R/ncstr.r",sep=""))

require(RJSONIO)

init <- function(options){

  opts <- fromJSON(options)
  organisms <- opts$referenceData$organisms

  # do disinfection
  t1 <- opts$treatOpts1
  t2 <- opts$treatOpts2
  disinfectRed1 <- opts$dis1
  disinfectRed2 <- opts$dis2
  prOfInf <- data.frame(crypto = c(0),giardia = c(0), rota = c(0), campy = c(0), eColi = c(0))
  annRisk <- data.frame(crypto = c(1:frameSize), giardia = c(1:frameSize), rota = c(1:frameSize), campy = c(1:frameSize), eColi = c(1:frameSize))
  sliceInfo <- data.frame(crypto = c(1:frameSize), giardia = c(1:frameSize), rota = c(1:frameSize), campy = c(1:frameSize), eColi = c(1:frameSize))
  for(org in organisms){
    name <- org$short
    optimParam <- c(org$optimizedParam, org$beta)
    totalLR <- opts$totalLR[[name]]
    respData <- DoseResponse(opts[[name]][["mean"]], opts[[name]][["sd"]], totalLR, org$model, optimParam)
    annRisk[name] <- AnnualRiskOfIllness(respData$sumProbs, org$illGivenInfect)
    sliceInfo[name] <-respData$normPDF
    prOfInf[name][1] <- sum(respData$wPrOfInf)
  }
  results <- list(annRiskIllness = annRisk, dpinf = prOfInf, normPDF = sliceInfo)
  return(toJSON(results))
}

Disinfect <- function(type, hrt, BF, temp, initConc, kValue, pH){
  if(type == "none"){
    return(0)
  } else {
    rc <- ReactorChar(hrt, BF, temp, pH, kValue, initConc)
    li <- LogInactivation(rc, temp, pH)
    ro <- RemainingOrganisms(rc, li)
    red <- data.frame(crypto=c(0),giardia=c(0),rota=c(0),campy=c(0),eColi=c(0))
    red$crypto[1] <- -log10(sum(ro[[type]][["crypto"]]) / 1E50)
    red$giardia[1] <- -log10(sum(ro[[type]][["giardia"]]) / 1E50)
    red$rota[1] <- -log10(sum(ro[[type]][["rota"]]) / 1E50)
    red$campy[1] <- -log10(sum(ro[[type]][["campy"]]) / 1E50)
    red$eColi[1] <- -log10(sum(ro[[type]][["eColi"]]) / 1E50)
    return(red)
  }
}

DoseResponse <- function(mean, sd, logRed, model, optimParam, water = 1.0){
  estLnMean <- EstimatedLnMean(mean, sd)
  estLnSD <- EstimatedLnSD(mean, sd)
  sliceProbs <- SliceProbabilities(defaultSlice, estLnMean, estLnSD)
  treatWater <- TreatedWaterPathConc(sliceProbs, logRed, water)
  sumProbs <- SumOfProbsForEachConc(sliceProbs,treatWater, model, optimParam)
  drData <- data.frame(sumProbs=sumProbs)
  drData$wPrOfInf <- WeightedDailyProbOfInf(drData$sumProbs, sliceProbs, model, optimParam)
  drData <- cbind(drData, sliceProbs)
  return(drData)
}
