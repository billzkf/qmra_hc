var app = new Vue({
  el: '#app',
  data: {
    title: "QMRA",
    running: false,
    output: {
      fresh: true,
      touched: false,
      prOfInf: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      },
      prOfInfYr: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      },
      prOfIllYr: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      },
      totalPrOfIllYr: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      },
      annDALYrisk: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      },
      annDALYtotal: {
        crypto: null,
        giardia: null,
        rota: null,
        campy: null,
        eColi: null
      }
    },
    population: 100000,
    dailyConsumption: 1,
    cryptoConc: 10,
    cryptoSD: 10,
    giardiaConc: 50,
    giardiaSD: 50,
    campyConc: 10000,
    campySD: 10000,
    rotaConc: 10,
    rotaSD: 10,
    eColiConc: 100000,
    eColiSD: 100000,
    eColiO157: 0.034,
    coagulation: "Coag Floc Sed",
    filtration: "Rapid Granular (coag/sed)",
    filtUser: false,
    coagUser: false,
    filt: {
      crypto: 0,
      giardia: 0,
      rota: 0,
      campy: 0,
      eColi: 0
    },
    coag: {
      crypto: 0,
      giardia: 0,
      rota: 0,
      campy: 0,
      eColi: 0
    },
    dis1: {
      plot: true,
      user: false,
      treat: "Free Chlorine",
      short: "freeChlorine",
      contactTime: 60,
      baffle: 0.5,
      initConc: 1,
      kValue: 0.002,
      temp: 10,
      pH: 6,
      logRed: {
        crypto: 0,
        giardia: 0,
        rota: 0,
        campy: 0,
        eColi: 0
      }
    },
    dis2: {
      plot: false,
      user: false,
      treat: "None",
      short: "none",
      contactTime: 60,
      baffle: 0.5,
      initConc: 1,
      kValue: 0.002,
      temp: 10,
      pH: 6,
      logRed: {
        crypto: 0,
        giardia: 0,
        rota: 0,
        campy: 0,
        eColi: 0
      }
    },
    treat3: "None",
    uvUser:false,
    uvLogRed: {
      crypto: 0,
      giardia: 0,
      rota: 0,
      campy: 0,
      eColi: 0
    },
    uvDose: 40,
    totalLogRed: {
      crypto: 0,
      giardia: 0,
      rota: 0,
      campy: 0,
      eColi: 0
    },
    organismsByName: {}
  },
  methods: {
    showOutput: function(data) {
      let result = JSON.parse(data.result);
      this.output.prOfInf.total = 0;
      this.output.prOfInfYr.total = 0;
      this.output.prOfIllYr.total = 0;
      this.output.totalPrOfIllYr.total = 0;
      this.output.annDALYrisk.total = 0;
      this.output.annDALYtotal.total = 0;
      for (let org in result.dpinf) {
        this.output.prOfInf[org] = result.dpinf[org][0];
        this.output.prOfInfYr[org] = this.probOfInfectionPerYear(result.dpinf[org][0]);
        this.output.prOfIllYr[org] = this.probOfIllnessPerYear(this.output.prOfInfYr[org], this.organismsByName[org].illGivenInfect);
        this.output.totalPrOfIllYr[org] = this.totalIllnessPerYear(this.output.prOfIllYr[org]);
        this.output.annDALYrisk[org] = this.indDALY(org, this.output.prOfIllYr[org]);
        this.output.annDALYtotal[org] = this.popDALY(org, this.output.prOfIllYr[org]);
        //totals
        this.output.prOfInf.total += this.output.prOfInf[org];
        this.output.prOfInfYr.total += this.output.prOfInfYr[org];
        this.output.prOfIllYr.total += this.output.prOfIllYr[org];
        this.output.totalPrOfIllYr.total += this.output.totalPrOfIllYr[org];
        this.output.annDALYrisk.total += this.output.annDALYrisk[org];
        this.output.annDALYtotal.total += this.output.annDALYtotal[org];
      }

      histogram('risk-inf', this.output.prOfInf, {
        title: 'Daily Risk of Infection'
      });
      histogram('risk-ill', this.output.prOfInfYr, {
        title: 'Daily Risk of Illness'
      });
      histogram('risk-ill-ann', this.output.prOfIllYr, {
        title: 'Annual Risk of Illness'
      });
      histogram('risk-ill-ann-total', this.output.totalPrOfIllYr, {
        title: 'Population Annual Risk of Illness'
      });
      histogram('risk-DALY', this.output.annDALYrisk, {
        title: 'Annual DALY Risk'
      });
      histogram('risk-DALY-total', this.output.annDALYtotal, {
        title: 'Annual Population DALY Risk'
      });
      riskPlot(result.annRiskIllness, result.normPDF, this.organismsByName);
    },
    probOfInfectionPerYear: function(dailyPrOI) {
      if (dailyPrOI < 10e-7) {
        return dailyPrOI * 365;
      }

      return 1 - Math.pow(1 - dailyPrOI, 365);
    },
    probOfIllnessPerYear: function(dailyPrOI, probOfIllGivenInf) {
      return dailyPrOI * probOfIllGivenInf;
    },
    totalIllnessPerYear: function(illnessPerYear) {
      return illnessPerYear * this.population;
    },
    indDALY: function(org, pIllYr) {
      return this.organismsByName[org].DALYs * pIllYr;
    },
    popDALY: function(org, pIllYr) {
      return this.organismsByName[org].DALYs * pIllYr * this.population;
    },
    /*
     * Disinfect
     * Start a worker to run the disinfection method calculation.
     */
    disinfect: function(dis, plotEl) {
      let results;
      let disKey = {
        "None": "none",
        "User Specified": "userSpecified",
        "Free Chlorine": "freeChlorine",
        "Chloramine": "chloramine",
        "Ozone": "ozone",
        "Chlorine Dioxide": "chlorineDioxide"
      };
      dis.short = disKey[dis.treat];
      dis.user = dis.short === 'userSpecified' ? true : false;
      dis.plot = dis.short === 'none' || dis.short === 'userSpecified' ? false : true;

      if (window.Worker) {
        var disWorker = new Worker('./treatment.js');
        disWorker.postMessage([dis]);
        disWorker.onmessage = function(e) {
          dis.logRed = e.data.reduction;
          app.totalLR();
          if (dis.plot) {
            disinfectPlot(e.data.reactChar, plotEl);
          }
        }
      } else {
        results = getReducedConc(dis.short, dis.contactTime, dis.baffle, dis.temp, dis.initConc, dis.kValue, dis.pH);
        dis.logRed = results.logRed;
        app.totalLR();
        if (dis.plot) {
          disinfectPlot(results.reactorChar, plotEl);
        }
      }
    },
    setCoag: function(){
      let cCoag = this.coagulation;
      let coag = this.refData.coagulation.filter((d) => {
        if (d.name === cCoag) {
          return true;
        }
        return false;
      });
      this.coag = coag[0];
      this.coagUser = this.coag.short === "userSpecified" ? true : false;
      this.coag.total = this.coag.crypto.est + this.coag.giardia.est + this.coag.rota.est + this.coag.campy.est + this.coag.eColi.est;
    },
    setFilt:function(){
      let cFilt = this.filtration;
      let filt = this.refData.filtration.filter((d) => {
        if (d.name === cFilt) {
          return true;
        }
        return false;
      });
      this.filt = filt[0];
      this.filtUser = this.filt.short === "userSpecified" ? true : false;
      this.filt.total = this.filt.crypto.est + this.filt.giardia.est + this.filt.rota.est + this.filt.campy.est + this.filt.eColi.est;
    },
    setUV:function(){
      this.uvUser = this.treat3 === "User Specified" ? true : false;
      if (this.treat3 === "UV") {
        let uvFormula = {
          crypto: 1.2344 * Math.log(this.uvDose) - 0.1283,
          giardia: 1.2085 * Math.log(this.uvDose) + 0.0715,
          rota: 0.102 * this.uvDose,
          campy: 0.880 * this.uvDose,
          eColi: 0.214 * this.uvDose
        };

        let uvMaxValue = {
          crypto: 5.0,
          giardia: 4.0,
          rota: 5.0,
          campy: 5.0,
          eColi: 5.5
        };

        this.uvLogRed.total = 0;
        for (var org in uvFormula) {
          if (this.uvDose > 0) {
            this.uvLogRed[org] = Math.min(uvMaxValue[org], uvFormula[org]);
            this.uvLogRed.total += this.uvLogRed[org];
          }
        }
      } else if(this.treat3 === "None"){
        this.uvLogRed = {
          crypto: 0,
          giardia: 0,
          rota: 0,
          campy: 0,
          eColi: 0
        };
      }
    },
    /*
     * Set treatments
     * Set the filtration, coagulation, and UV log reduction.
     */
    setTreatments: function() {
      this.setCoag();
      this.setFilt();
      this.setUV();
      this.totalLR();
    },
    totalLR: function() {
      let valid = true
      for (let org in this.totalLogRed) {
        this.totalLogRed[org] = this.uvLogRed[org] + this.coag[org].est + this.filt[org].est + this.dis1.logRed[org] + this.dis2.logRed[org];
        valid = isNaN(this.totalLogRed[org]) || typeof this.totalLogRed === 'undefined' ? false : true;
        if(!valid) break;
      }
      if(valid){
        stackedBar({
          uv: this.uvLogRed,
          coag: this.coag,
          filt: this.filt,
          dis1: this.dis1.logRed,
          dis2: this.dis2.logRed
        });
      } else {
        throw new Error("There was an error calculating the log reduction.");
      }
    },
    sendData: function(event) {
      this.running = true;
      this.setTreatments();
      let json = {
        population: this.population * 1,
        consumption: this.dailyConsumption * 1,
        crypto: {
          mean: this.cryptoConc * 1,
          sd: this.cryptoSD * 1
        },
        giardia: {
          mean: this.giardiaConc * 1,
          sd: this.giardiaSD * 1
        },
        rota: {
          mean: this.rotaConc * 1,
          sd: this.rotaSD * 1
        },
        campy: {
          mean: this.campyConc * 1,
          sd: this.campySD * 1
        },
        eColi: {
          mean: this.eColiConc * 1 * this.eColiO157,
          sd: this.eColiSD * 1 * this.eColiO157
        },
        coagulation: this.coag,
        filtration: this.filt,
        treatOpts1: {
          type: this.treatType1,
          hrt: this.contactTime1 * 1,
          BF: this.baffle1 * 1,
          initConc: this.initConc1 * 1,
          kValue: this.kValue1 * 1,
          temp: this.temp1 * 1,
          pH: this.pH1 * 1
        },
        treatOpts2: {
          type: this.treatType2,
          hrt: this.contactTime2 * 1,
          BF: this.baffle2 * 1,
          initConc: this.initConc2 * 1,
          kValue: this.kValue2 * 1,
          temp: this.temp2 * 1,
          pH: this.pH2 * 1
        },
        treatOpts3: {
          uv: this.treat3,
          uvDose: this.uvDose
        },
        totalLR: this.totalLogRed,
        dis1: this.dis1.logRed,
        dis2: this.dis2.logRed,
        referenceData: this.refData
      };

      let string = JSON.stringify(json);
      this.$http.post('./all', string).then(function(res) {
        this.output.fresh = false;
        this.output.touched = false;
        this.running = false;
        this.showOutput(res.body);
      })
    }
  },
  created: function() {
    let refData;
    let orgs;
    this.$http.get("reference_data.json")
      .then(function(response) {
        this.refData = response.body;
        this.$http.get("organisms.json")
          .then(function(response) {
            this.refData.organisms = response.body.organisms;
            this.refData.organisms.forEach((d) => {
              this.organismsByName[d.short] = d;
            });
            this.disinfect(this.dis1, 'dis-plot-1');
            this.disinfect(this.dis2, 'dis-plot-2');
            this.setTreatments();
          });
      });
  }
});
