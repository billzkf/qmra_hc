Math.log10 = Math.log10 || function(n) {
  return Math.log(n) / Math.log(10);
}

var orgNames = ['Cryptosporidium', 'Giardia', 'Rota', 'Campylobacter', 'E. Coli'];

var disinfectPlot = function(reactorChar, el) {
  var resid = {
    x: reactorChar.time,
    y: reactorChar.residual,
    mode: 'lines',
    name: 'residuals'
  };

  var cdf = {
    x: reactorChar.time,
    y: reactorChar.CDF,
    mode: 'lines',
    name: 'cdf'
  };
  Plotly.newPlot(el, [resid, cdf], {
    showlegend: true,
    legend: {
      orientation: 'h'
    },
    xaxis:{
      side:'top',
      title:'Hydraulic Residence Time'
    },
    yaxis:{
      title:'cdf'
    }
  }, {
    displaylogo: false
  });
};

var histogram = function(el, data, layout) {
  var values = [];
  for (var org in data) {
    if (org !== "total"){
      values.push(data[org]);
    }
  }
  var trace = {
    x: orgNames,
    y: values,
    marker: {
      color: ['rgba(193,66,66,1)',
        'rgba(234,158,71,1)',
        'rgba(124,226,48,1)',
        'rgba(48,155,226,1)',
        'rgba(165,109,221,1)'
      ]
    },
    type: 'bar'
  }

  layout.yaxis = {
    type: 'log',
    tickformat: 'e{4}',
    range: [-15, 0]
  };
  Plotly.newPlot(el, [trace], layout, {
    displaylogo: false
  });
}

var stackedBar = function(trts) {

  var data = [{
    x: orgNames,
    y: [trts.uv.crypto, trts.uv.giardia, trts.uv.rota, trts.uv.campy, trts.uv.eColi],
    name: 'UV',
    type: 'bar'
  }, {
    x: orgNames,
    y: [trts.coag.crypto.est, trts.coag.giardia.est, trts.coag.rota.est, trts.coag.campy.est, trts.coag.eColi.est],
    name: 'Coagulation',
    type: 'bar'
  }, {
    x: orgNames,
    y: [trts.filt.crypto.est, trts.filt.giardia.est, trts.filt.rota.est, trts.filt.campy.est, trts.filt.eColi.est],
    name: 'Filtration',
    type: 'bar'
  }, {
    x: orgNames,
    y: [trts.dis1.crypto, trts.dis1.giardia, trts.dis1.rota, trts.dis1.campy, trts.dis1.eColi],
    name: 'Disinfect 1',
    type: 'bar'
  }, {
    x: orgNames,
    y: [trts.dis2.crypto, trts.dis2.giardia, trts.dis2.rota, trts.dis2.campy, trts.dis2.eColi],
    name: 'Disinfect 2',
    type: 'bar'
  }];

  var layout = {
    barmode: 'stack',
    yaxis:{
      title:'Log Reduction'
    }
  };

  //check if element exists to prevent error on load.
  if(document.getElementById('reduction-plot') !== null){
    Plotly.newPlot('reduction-plot', data, layout, {
      displaylogo: false
    });
  }
}

var riskPlot = function(annRisk, npdf, organisms) {

  var data = [{
    x: [1e-4, 1e-4],
    y: [0, 1],
    mode: 'lines',
    name: 'US-EPA risk level',
    line: {
      dash: 'dashdot',
      width: 4
    }
  }, {
    x: annRisk.crypto,
    y: npdf.crypto,
    mode: 'lines',
    name: 'Cryptosporidium'
  }, {
    x: annRisk.giardia,
    y: npdf.giardia,
    mode: 'lines',
    name: 'Giardia'
  }, {
    x: annRisk.rota,
    y: npdf.rota,
    mode: 'lines',
    name: 'Rotavirus'
  }, {
    x: annRisk.campy,
    y: npdf.campy,
    mode: 'lines',
    name: 'Campylobacter'
  }, {
    x: annRisk.eColi,
    y: npdf.eColi,
    mode: 'lines',
    name: 'E. Coli'
  }]

  var DALYData = [{
    x: [1e-6, 1e-6],
    y: [0, 1],
    mode: 'lines',
    name: 'Health Canada risk level',
    line: {
      dash: 'dashdot',
      width: 4
    }
  }, {
    x: annRisk.crypto.map(function(d) {
      return d * organisms.crypto.DALYs;
    }),
    y: npdf.crypto,
    mode: 'lines',
    name: 'Cryptosporidium'
  }, {
    x: annRisk.giardia.map(function(d) {
      return d * organisms.giardia.DALYs;
    }),
    y: npdf.giardia,
    mode: 'lines',
    name: 'Giardia'
  }, {
    x: annRisk.rota.map(function(d) {
      return d * organisms.rota.DALYs;
    }),
    y: npdf.rota,
    mode: 'lines',
    name: 'Rotavirus'
  }, {
    x: annRisk.campy.map(function(d) {
      return d * organisms.campy.DALYs;
    }),
    y: npdf.campy,
    mode: 'lines',
    name: 'Campylobacter'
  }, {
    x: annRisk.eColi.map(function(d) {
      return d * organisms.eColi.DALYs;
    }),
    y: npdf.eColi,
    mode: 'lines',
    name: 'E. Coli'
  }]

  var layout = {
    displaylogo: false,
    xaxis: {
      title: 'Annual Risk of Infection',
      type: 'log',
      tickformat: 'e{4}',
      autorange: true
    },
    autorange: true
  };

  var layoutDALY = {
    displaylogo: false,
    xaxis: {
      title: 'Probability Distribution for Annual DALY Risk',
      type: 'log',
      tickformat: 'e',
      autorange: true
    },
    autorange: true
  };

  Plotly.newPlot('output-plot-1', data, layout);
  Plotly.newPlot('output-plot-2', DALYData, layoutDALY);
}
