# QMRA Model probabilistic model to estimate annual risk of illness & DALYs based on source water pathogens and treatment barriers

_Health Canada - I.Douglas/J.Elliott/S.McFadyen/T.Brooks - April(2007); revised October(2008); December(2010); June(2011); October(2011); May(2013); March(2014); May(2015)_

Reference values and equations used in the QMRA calculations<br>
User specified values<br>
"active" values used in model calculations

## Overview of calculation method used by model:

_General: calculations are performed for each pathogen separately, and are also summated to determine overall illnesses and DALYs_

- Pathogen concentrations are fitted to a log-normal distribution based on mean & standard deviation values in source water data<br>
  NOTE: values for mean of ln(x) and std-dev. of ln(x) are estimated from arithmetic mean (x) & std-dev (STD) of source data using following equations:<br>
  mean of ln(x) = ln [ x2 / SQRT(x2 + STD2) ]<br>
  std-dev of ln(x) = SQRT [ ln ((STD/x)2 + 1) ]
- The log-normal distribution is divided into 500 'slices' for integration, each representing a possible 'test' concentration. Each 'slice' has equal probability weighting.
- For each source water 'test' concentration, the mean treated water concentration is calculated using the overall log-reduction value for treatment: Conc(treat) = Conc.(raw) x 10 - (log reduction)
- The treated water pathogen concentration is multiplied by the daily water consumption to estimate the mean # of organisms in the daily water consumed
- A Poisson probability distribution is used to estimate the probability of consuming a discrete number of pathogens based on the mean # of pathogens in daily water consumed<br>
  (ie: chance of consuming exactly 0 pathogens, 1 pathogen, 2 pathogens... etc. up to 100 pathogens, given a mean concentration in the water per day)
- The probability of infection is calculated using the specific pathogen dose-response model for each ingestion dose ranging from 0 - 100 per day
- The daily probability of infection Pinf,daily for a given 'test' concentration is the result of the sum of: Σ [ Pinf/dose x Pdose ] = sum of prob's for dose range of 0 - 100 ingested per day
- The probability of illness is calculated by multiplying the P(inf,daily) x P(illness given infection);
- The annual probability of illness is calculated using the equation: P(ill, yr) = 1 - [1 - P(ill, daily)]N<br>
  xi) DALY's are comprehensive health index values that estimate the impact of an illness based on duration and severity of effects - see below;
- DALY risk is calculated by multiplying P(illness) x (DALYs per case of illness);
- Estimated Total # illnesses & Total DALYs are calculated by multiplying the individual risk values times the population

Source water pathogen concentrations

- reference values for pathogen concentrations based on qualitative description of source water
- values are approximations based on available sets of pathogen data from several Canadian source waters (2007)
- reference values should only be used in the absence of site-specific pathogen data
- for standard deviation, user can assume STD-DEV is equal to MEAN values as a reasonable estimate of source water variability

### Typical source water pathogen concentrations (# per 100 Litres)

| Crypto | Giardia | Rotavirus | Campy  | Ecoli
------------------- | ------ | ------- | --------- | ------ | -------
Pristine            | 0.1    | 0.5     | 0.1       | 100    | 100
Lightly Impacted    | 1      | 5       | 1         | 1000   | 10000
Moderately Impacted | 10     | 50      | 10        | 10000  | 100000
Heavily Impacted    | 100    | 500     | 100       | 100000 | 1000000

_Definitions for these source water categories are listed below for guidance:_

- pristine - no evidence of human impact or fecal contamination (eg. protected groundwater source)
- lightly impacted - minimal human settlement or agricultural activities; no direct input of human or livestock wastes (eg. protected reservoir / lake)
- moderately impacted - human settlement and/or agricultural activity in watershed; intake not under direct influence of wastewater discharges (eg. rivers, lakes)
- heavily impacted - intensive human / agriculture activities in watershed; direct discharge of wastes (eg. small river in urban / agricultural area)

EcoliO157 as a percentage of total Ecoli

- concentrations of Ecoli_O157 can be rougly estimated based on total Ecoli concentrations measured in source water
- a reference value of Ecoli_O157 = 3.4% of total Ecoli is based on a values cited in literature (Martins et al., 1992)
- NOTE: if data is available for Ecoli_O157 specifically, then the mean and std-dev values should be entered directly on the input/output page and assumed ratio of Ecoli_O157 changed to 100%

| mean   | std-dev
------------------------------------------- | ------ | ---------------------
Total Ecoli reported values entered:        | 100000 | 100000 (no. per 100L)
assumed ratio of Ecoli_O157 to Total Ecoli: | 3.4%   | 3.4%
assumed conc. of Ecoli_O157 in raw water:   | 3400   | 3400 (no. per 100L)

## Treatment - physical log removal

- log removal values for each treatment stage are weighted mean values taken from a large literature survey (Hijnen and Medema, 2007)
- log removal estimates for membrane filtration (microfiltration & ultrafiltration) were based on a literature review by Health Canada (2008)
- Hijnen and Medema (2007) examined the quality of each literature value and assinged a weighting factor accordingly (scale of 1 to 5);
- higher quality data were assigned greater weighting factors (eg. full-scale data better than pilot plant; pathogen data better than surrogate particles, etc.)
- weighted standard deviation was calculated from Hijnen and Medema (2007) data set and weighting factors using following formula:<br>
  (<http://www.itl.nist.gov/div898/software/dataplot/refman2/ch2/weightsd.pdf>)
- regular standard deviation was calculated for log removal values in membrane filtration data set (Health Canada)
- model uses (weighted mean) values of physical log removal and reports a mean estimate;

- where two physical removal processes are combined, the model estimates the standard deviation for the combined processes as: SQRT [STDEV12 + STDEV22]

- log removal literature data and statistically derived values are presented in tables below

### Coag. & Sedimentation Estimated Log Removal

**Study #**      | **Crypto** | **Giardia** | **Rotavirus** | **Campy** | **E. Coli**
:-------------------: | :--------: | :---------: | :-----------: | :-------: | :---------:
          1           |    3.8     |     2.9     |      4.3      |    3.7    |     3.7
          2           |    3.7     |     2.9     |      3.1      |    3.4    |     3.4
          3           |    3.1     |     2.6     |      3.0      |    3.0    |     3.0
          4           |    2.6     |     2.4     |      3.0      |    1.9    |     1.9
          5           |    2.5     |     2.1     |      2.5      |    1.9    |     1.9
          6           |    2.3     |     1.6     |      2.3      |    1.9    |     1.9
          7           |    2.1     |     1.3     |      2.3      |    1.8    |     1.8
          8           |    2.1     |     1.3     |      2.2      |    1.8    |     1.8
          9           |    2.1     |     1.1     |      2.0      |    1.4    |     1.4
         10           |    2.0     |     1.0     |      1.9      |    1.4    |     1.4
         11           |    2.0     |     0.7     |      1.9      |    1.2    |     1.2
         12           |    1.8     |     0.6     |      1.7      |    1.2    |     1.2
         13           |    1.6     |     0.3     |      1.7      |    0.9    |     0.9
         14           |    1.4     |             |      1.5      |    0.8    |     0.8
         15           |    1.3     |             |      1.5      |    0.8    |     0.8
         16           |    1.3     |             |      1.3      |    0.7    |     0.7
         17           |    1.3     |             |      1.2      |    0.6    |     0.6
         18           |    1.2     |             |      1.1      |    0.6    |     0.6
         19           |    1.0     |             |      1.0      |    0.6    |     0.6
         20           |    1.0     |             |      0.9      |           |
         21           |    0.9     |             |      0.6      |           |
         22           |    0.7     |             |      0.5      |           |
         23           |    0.4     |             |      0.3      |           |
         24           |            |             |      0.2      |           |
         25           |            |             |      0.2      |           |
     **Wt. Avg**      |  **1.86**  |  **1.61**   |   **1.76**    | **1.55**  |   **1.6**
     Wt, Std Dev      |    0.94    |    0.94     |     0.97      |   0.95    |     0.9
Wt. Avg + Wt Std Dev  |    2.80    |    2.55     |     2.74      |   2.50    |    2.50
Wt. Avg - Wt. Std Dev |    0.92    |    0.67     |     0.79      |   0.60    |    0.60
        Mean          |    1.83    |    1.60     |     1.69      |   1.56    |     1.6
      Std. Dev        |    0.89    |    0.89     |     1.02      |   0.94    |     0.9
       Median         |    1.80    |    1.30     |     1.70      |   1.40    |     1.4

### Granular Filt. (no coag) Estimated Log Removal

Study #        | Crypto | Giardia | Rotavirus | Campy | E. Coli
:-------------------: | :----: | :-----: | :-------: | :---: | :-----:
          1           |  2.3   |   2.6   |    3.8    |  1.0  |   1.0
          2           |  2.2   |   2.0   |    1.6    |  1.0  |   1.0
          3           |  2.1   |   1.5   |    1.5    |  0.9  |   0.9
          4           |  1.5   |   1.2   |    1.2    |  0.7  |   0.7
          5           |  1.3   |   0.8   |    1.1    |  0.6  |   0.6
          6           |  1.2   |   0.4   |    0.9    |  0.5  |   0.5
          7           |  1.1   |   0.2   |    0.8    |  0.4  |   0.4
          8           |  1.1   |   0.0   |    0.8    |  0.4  |   0.4
          9           |  1.1   |         |    0.7    |  0.4  |   0.4
         10           |  1.0   |         |    0.6    |  0.3  |   0.3
         11           |  0.7   |         |    0.6    |  0.2  |   0.2
         12           |  0.7   |         |    0.6    |  0.2  |   0.2
         13           |  0.5   |         |    0.6    |  0.2  |   0.2
         14           |  0.4   |         |    0.5    |       |
         15           |  0.3   |         |    0.5    |       |
         16           |  0.2   |         |    0.4    |       |
         17           |  0.0   |         |    0.3    |       |
         18           |        |         |    0.3    |       |
         19           |        |         |    0.2    |       |
         20           |        |         |    0.2    |       |
         21           |        |         |    0.2    |       |
         22           |        |         |    0.1    |       |
         23           |        |         |    0.0    |       |
         24           |        |         |           |       |
         25           |        |         |           |       |
         26           |        |         |           |       |
         27           |        |         |           |       |
      **Wt. Avg**        |  **1.11**  |  **1.23**   |  **0.77**    | **0.55**  |   **0.5**
     Wt, Std Dev      |  0.71  |  0.94   |   0.78    | 0.29  |   0.3
Wt. Avg + Wt Std Dev  |  1.82  |  2.17   |   1.55    | 0.84  |  0.84
Wt. Avg - Wt. Std Dev |  0.39  |  0.28   |   0.00    | 0.26  |  0.26
        Mean          |  1.04  |  1.09   |   0.76    | 0.52  |   0.5
      Std. Dev        |  0.69  |  0.91   |   0.79    | 0.29  |   0.3
       Median         |  1.10  |  1.00   |   0.60    | 0.40  |   0.4

### Granular Filt. (inline)

Study #        | Crypto | Giardia | Rotavirus | Campy | E. Coli
:-------------------: | :----: | :-----: | :-------: | :---: | :-----:
          1           |  5.4   |   4.7   |    1.5    |  2.1  |   2.1
          2           |  5.3   |   4.6   |    1.5    |  1.6  |   1.6
          3           |  5.2   |   4.3   |    1.0    |  1.5  |   1.5
          4           |  5.1   |   3.9   |    0.9    |  1.4  |   1.4
          5           |  4.9   |   3.4   |    0.8    |  1.1  |   1.1
          6           |  4.6   |   3.4   |    0.4    |  1.0  |   1.0
          7           |  4.5   |   3.3   |    0.3    |  0.8  |   0.8
          8           |  4.3   |   3.3   |    0.3    |       |
          9           |  4.1   |   3.3   |    0.2    |       |
         10           |  4.1   |   3.0   |    0.2    |       |
         11           |  3.7   |   3.0   |    0.2    |       |
         12           |  3.0   |   2.9   |    0.2    |       |
         13           |  2.9   |   2.8   |    0.1    |       |
         14           |  2.9   |   2.7   |           |       |
         15           |  2.9   |   2.4   |           |       |
         16           |  2.9   |   2.0   |           |       |
         17           |  2.8   |   1.7   |           |       |
         18           |  2.7   |   1.5   |           |       |
         19           |  2.7   |   1.4   |           |       |
         20           |  2.6   |   1.3   |           |       |
         21           |  2.5   |   0.8   |           |       |
         22           |  2.3   |         |           |       |
         23           |  2.2   |         |           |       |
         24           |  2.1   |         |           |       |
         25           |  1.9   |         |           |       |
         26           |  1.5   |         |           |       |
         27           |  1.5   |         |           |       |
         28           |  1.0   |         |           |       |
         29           |  0.8   |         |           |       |
         30           |  0.1   |         |           |       |
       Wt. Avg        |  2.97  |  2.86   |   0.59    | 1.36  |  1.36
     Wt, Std Dev      |  1.33  |  1.08   |   0.47    | 0.44  |  0.44
Wt. Avg + Wt Std Dev  |  4.30  |  3.94   |   1.06    | 1.79  |  1.79
Wt. Avg - Wt. Std Dev |  1.65  |  1.78   |   0.11    | 0.92  |  0.92
        Mean          |  3.08  |  2.84   |   0.58    | 1.36  |  1.36
      Std. Dev        |  1.42  |  1.09   |   0.50    | 0.44  |  0.44
       Median         |  2.90  |  3.00   |   0.30    | 1.40  |  1.40

_Elimination of microorganisms by drinking water treatment processes (Hijnen and Medema, 2007)_

### Granular Filt. (coag/sed) Estimated Log Removal

Study #               | Crypto | Giardia | Rotavirus | Campy | E. Coli
--------------------- | ------ | ------- | --------- | ----- | -------
1                     | 2.3    | 2.6     | 3.8       | 1.0   | 1.0
2                     | 2.2    | 2.0     | 1.6       | 1.0   | 1.0
3                     | 2.1    | 1.5     | 1.5       | 0.9   | 0.9
4                     | 1.5    | 1.2     | 1.2       | 0.7   | 0.7
5                     | 1.3    | 0.8     | 1.1       | 0.6   | 0.6
6                     | 1.2    | 0.4     | 0.9       | 0.5   | 0.5
7                     | 1.1    | 0.2     | 0.8       | 0.4   | 0.4
8                     | 1.1    | 0.0     | 0.8       | 0.4   | 0.4
9                     | 1.1    |         | 0.7       | 0.4   | 0.4
10                    | 1.0    |         | 0.6       | 0.3   | 0.3
11                    | 0.7    |         | 0.6       | 0.2   | 0.2
12                    | 0.7    |         | 0.6       | 0.2   | 0.2
13                    | 0.5    |         | 0.6       | 0.2   | 0.2
14                    | 0.4    |         | 0.5       |       |
15                    | 0.3    |         | 0.5       |       |
16                    | 0.2    |         | 0.4       |       |
17                    | 0.0    |         | 0.3       |       |
18                    |        |         | 0.3       |       |
19                    |        |         | 0.2       |       |
20                    |        |         | 0.2       |       |
21                    |        |         | 0.2       |       |
22                    |        |         | 0.1       |       |
23                    |        |         | 0.0       |       |
24                    |        |         |           |       |
25                    |        |         |           |       |
26                    |        |         |           |       |
27                    |        |         |           |       |
Wt. Avg               | 1.11   | 1.23    | 0.77      | 0.55  | 0.5
Wt, Std Dev           | 0.71   | 0.94    | 0.78      | 0.29  | 0.3
Wt. Avg + Wt Std Dev  | 1.82   | 2.17    | 1.55      | 0.84  | 0.84
Wt. Avg - Wt. Std Dev | 0.39   | 0.28    | 0.00      | 0.26  | 0.26
Mean                  | 1.04   | 1.09    | 0.76      | 0.52  | 0.5
Std. Dev              | 0.69   | 0.91    | 0.79      | 0.29  | 0.3
Median                | 1.10   | 1.00    | 0.60      | 0.40  | 0.4

_Elimination of microorganisms by drinking water treatment processes (Hijnen and Medema, 2007)_

### Slow Sand Filtration Estimated Log Removal

Study #               | Crypto | Giardia | Rotavirus | Campy | E. Coli
--------------------- | ------ | ------- | --------- | ----- | -------
1                     | 6.5    | 6.0     | 4.0       | 4.8   | 4.8
2                     | 5.7    | 4.7     | 3.5       | 4.6   | 4.6
3                     | 5.3    | 4.0     | 3.4       | 3.9   | 3.9
4                     | 4.7    |         | 3.0       | 3.4   | 3.4
5                     | 4.5    |         | 2.8       | 3.2   | 3.2
6                     | 2.7    |         | 2.1       | 2.9   | 2.9
7                     |        |         | 2.1       | 2.8   | 2.8
8                     |        |         | 1.9       | 2.5   | 2.5
9                     |        |         | 1.9       | 2.4   | 2.4
10                    |        |         | 1.3       | 2.2   | 2.2
11                    |        |         | 0.9       | 2.2   | 2.2
12                    |        |         | 0.7       | 2.0   | 2.0
13                    |        |         | 0.6       | 1.9   | 1.9
14                    |        |         |           | 1.9   | 1.9
15                    |        |         |           | 1.6   | 1.6
16                    |        |         |           | 1.3   | 1.3
17                    |        |         |           | 1.2   | 1.2
Wt. Avg               | 4.66   | 4.88    | 2.18      | 2.69  | 2.69
Wt, Std Dev           | 1.32   | 0.97    | 1.09      | 1.08  | 1.08
Wt. Avg + Wt Std Dev  | 5.98   | 5.85    | 3.27      | 3.77  | 3.77
Wt. Avg - Wt. Std Dev | 3.33   | 3.91    | 1.09      | 1.60  | 1.60
Mean                  | 4.90   | 4.90    | 2.17      | 2.64  | 2.64
Std. Dev              | 1.30   | 1.01    | 1.11      | 1.06  | 1.06
Median                | 5.00   | 4.70    | 2.10      | 2.40  | 2.40

_Elimination of microorganisms by drinking water treatment processes (Hijnen and Medema, 2007)_

### Microfiltration (membrane) Estimated Log Removal References:

Study #  | Crypto | Giardia | Rotavirus | Campy | E. Coli
-------- | ------ | ------- | --------- | ----- | -------
1        | 6.8    | 6.4     | 0.5       | 3.9   | 3.9
2        | 6.9    | 6.9     | 0.0       | 6.7   | 6.7
3        | 6.1    | 6.9     | 2.0       | 6.5   | 6.5
4        | 6.0    | 6.7     | 0.5       | 2.8   | 2.8
5        | 6.3    | 7.0     | 2.5       | 3.1   | 3.1
6        | 6.6    | 5.8     |           |       |
7        | 4.1    |         |           |       |
8        | 5.6    |         |           |       |
9        | 6.8    |         |           |       |
Mean     | 6.13   | 6.62    | 1.10      | 4.60  | 4.60
Std dev. | 0.88   | 0.45    | 1.08      | 1.87  | 1.87
Median   | 6.30   | 6.80    | 0.50      | 3.90  | 3.90

_Health Canada literature review (2008)_

References:

- Cryptosporidium

  - 1-5 Jacangelo JG, Adham SS, Laine J-M (1997)
  - 6-8 NSF, 2002
  - 9 NSF 2000a

- Giardia

  - 1-5 Jacangelo JG, Adham SS, Laine J-M (1997)
  - 6 NSF 2000a

- Virus

  - 1-5 Jacangelo JG, Adham SS, Laine J-M (1997)

- E.coli

  - 1-3 NSF, 2002
  - 4-5 NSF, 2003

### Ultrafiltration (membrane) Estimated Log Removal

Study #  | Crypto | Giardia | Rotavirus | Campy | E. Coli
-------- | ------ | ------- | --------- | ----- | -------
1        | 6.9    | 6.4     | 3.9       | 8.0   | 8.0
2        | 6.7    | 7.0     | 4.7       |       |
3        | 7.0    | 6.7     | 3.4       |       |
4        | 6.4    | 6.7     | 4.3       |       |
5        | 6.3    | 6.9     | 4.0       |       |
6        | 6.4    | 5.3     | 5.7       |       |
7        | 6.5    | 5.5     | 2.9       |       |
8        | 5.8    | 4.9     | 4.3       |       |
9        | 5.7    |         | 4.9       |       |
10       |        |         | 4.9       |       |
11       |        |         | 3.6       |       |
12       |        |         | 3.6       |       |
13       |        |         | 3.3       |       |
14       |        |         | 5.5       |       |
15       |        |         | 5.8       |       |
16       |        |         | 1.7       |       |
17       |        |         | 2.1       |       |
18       |        |         | 4.8       |       |
19       |        |         | 4.8       |       |
Mean     | 6.41   | 6.18    | 4.12      | 8.00  | 8.00
Std dev. | 0.44   | 0.82    | 1.13      |       |
Median   | 6.40   | 6.55    | 4.30      | 8.00  | 8.00

_Health Canada literature review (2008)_

References:

- Cryptosporidium

  - 1-5 Jacangelo JG, Adham SS, Laine J-M (1997)NSF 2000e
  - 6 NSF 2000e
  - 7 NSF 2000b
  - 8 NSF 2000c
  - 9 NSF 2006

- Giardia

  - 1-5 Jacangelo JG, Adham SS, Laine J-M (1997)
  - 6 NSF 2000e
  - 7 NSF 2000b
  - 8 NSF 2000c

- Virus

  - 1-3 NSF 2000f
  - 5-8 NSF 2000g
  - 9-10 NSF 2006
  - 11-13 NSF 2001
  - 14-17 NSF 2000d
  - 18-19 NSF 2006 E.coli

- decision to consider as absolute barrier for practical operating concentrations, citing results ofraw water influent challenge studies with Brevunidmonas dimunata (NSF,2006) in absence of sound E.coli seeding study data;

### Treatment - chemical & UV disinfection

- chemical & UV inactivation values were based on literature and reference values noted below
- safety factors used in C-T tables were removed unless there was a scientific basis for their inclusion - as noted below;
- equations were curve-fitted to reported data values for use in the model calculations
- where C-T values exceeded reported literature values, maximum log-Inactivation levels were "capped" as noted below
- in the absence of experimental data, inactivation was assumed to be first-order (ie. Log-Inactivation proportional to CT values)
- in the absence of experimental data, inactivation rate was assumed to double for every 10 deg.C rise in temperature

Crypto – Free Chlorine<br>
Korich 1990<br>
2-log demonstrated @ pH 7, 25°C; CT = 7200 mg/L-min<br>
level of inactivation calculated by model is capped at 4-log

Crypto – Chloramine<br>
Finch, G. R., Gyurek, L. L., & Liyanage, L. R. (1997).<br>
Hom model using k=1.1x10-6 n=2.53 m=1.28<br>
2.3-log demonstrated at pH=8, 22ºC;<br>
level of inactivation calculated by model is capped at 5-log

Crypto – Ozone<br>
EPA (2006)<br>
less conservative model than previous EPA guidance documents<br>
EPA applied 90% confidence boundary to the data set<br>
no sufficient evidence for inactivation beyond 3-log, although Finch(1996) observed 4.7-log with CT=36 at pH=6.9 and 22ºC<br>
level of inactivation calculated by model is capped at 6-log

Crypto – Chlorine Dioxide<br>
EPA (2006)<br>
less conservative model than previous EPA guidance documents<br>
EPA applied 90% confidence boundary to the data set<br>
3.0-log inactivation demonstrated at pH=8 and 22ºC<br>
level of inactivation calculated by model is capped at 6-log

Crypto – Ultraviolet<br>
Hijnen, 2006.<br>
some tailing observed at 3-log, although other researchers observed up to 5-log<br>
level of inactivation calculated by model is capped at 5-log

Giardia – Free Chlorine<br>
curve fit to data from EPA SWTR (1999)<br>
no safety factors applied to CT tables, but used 99% UCL statistical fit of experimental data<br>
4-log inactivation demonstrated at pH=6-9<br>
level of inactivation calculated by model is capped at 8-log

Giardia – Chloramine<br>
curve fit to data from EPA SWTR (1999)<br>
2-log inactivation demonstrated at pH=8<br>
level of inactivation calculated by model is capped at 4-log

Giardia – Ozone<br>
curve fit to data from EPA SWTR (1999)<br>
safety factor of 2 in EPA table - removed for model calc's<br>
2-log inactivation demonstrated at 5ºC, pH 7<br>
level of inactivation calculated by model is capped at 4-log

Giardia – Chlorine Dioxide<br>
Curve fit to data from EPA SWTR (1999)<br>
safety factor of 1.5 in EPA table - removed for model calc's<br>
2-log inactivation demonstrated<br>
level of inactivation calculated by model is capped at 4-log

Giardia – Ultraviolet<br>
Hijnen, 2006<br>
3.0-log inactivation demonstrated, but with tailing (Craik, 2000); several observations cited at 4-log<br>
level of inactivation calculated by model is capped at 4-log

Virus – Free Chlorine<br>
curve fit to original data from Sobsey, 1988 (Hepatitis A virus); data cited in EPA SWTR (1999)<br>
NOTE: For information, US-EPA CT tables included a safety factor of 3<br>
NOTE: CT values based on HAV although Rotavirus may be 20-25 times more easily inactivated<br>
4-log inactivation demonstrated at 5ºC, pH 6-9<br>
level of inactivation calculated by model is capped at 8-log

Virus – Chloramine<br>
curve fit to data from EPA SWTR (1999)<br>
NOTE: CT values were based on HAV; Rotavirus may be 5-times more resistant than HAV<br>
2-log inactivation demonstrated at 5ºC, pH=7<br>
level of inactivation calculated by model is capped at 4-log

Virus – Ozone<br>
curve fit to data from EPA SWTR (1999)<br>
safety factor of 3 in EPA table - removed for model calc's<br>
NOTE: CT values based on Poliovirus1, but Rotavirus may be 3-30 times more easily inactivated<br>
2-log inactivation demonstrated at 5ºC, pH 7<br>
level of inactivation calculated by model is capped at 4-log

Virus – Chlorine Dioxide<br>
curve fit to data from EPA SWTR (1999)<br>
safety factor of 2 in EPA table - removed for model calc's<br>
NOTE: CT values based on HAV though Rotavirus may be 20-times more easily inactivated<br>
4-log inactivation demonstrated at 5ºC, pH 7<br>
level of inactivation calculated by model is capped at 8-log

Virus – Ultraviolet<br>
Hijnen, 2006<br>
5.0-log inactivation demonstrated, but with tailing;<br>
level of inactivation calculated by model is capped at 5-log

Campylobacter – Free Chlorine<br>
Blaser,1986<br>
used average values across all temperature & pH conditions cited<br>
proportional based on 3.64-log at CT=0.5 mg/L-min<br>
3.6-log inactivation demonstrated at various pH and temperature values<br>
level of inactivation calculated by model is capped at 8-log

Campylobacter – Chloramine<br>
Blaser,1986<br>
used average values across all temperature & pH conditions cited<br>
proportional based on 3.77-log at CT=15 mg/L-min<br>
3.8-log inactivation demonstrated at various pH and temperature values<br>
level of inactivation calculated by model is capped at 8-log

Campylobacter – Ozone<br>
use CT values for E.Coli (no Campylobacter studies available)<br>
demonstrated 5.0-log inactivation<br>
level of inactivation calculated by model is capped at 8-log

Campylobacter – Chlorine Dioxide<br>
use CT values for E.Coli (no Campylobacter studies available)<br>
demonstrated 2.0-log inactivation<br>
level of inactivation calculated by model is capped at 4-log

Campylobacter – Ultraviolet<br>
Hijnen, 2006<br>
combined result from Butler 1986, and Wilson 1992.<br>
4.5 - 5.0 log inactivation demonstrated in studies<br>
level of inactivation calculated by model is capped at 5-log

E.Coli – Free Chlorine<br>
Rice and Clark, 1999<br>
curve fit for 7 serotypes of EcoliO157<br>
demonstrated 4.5-log inactivation<br>
level of inactivation calculated by model is capped at 8-log

E.Coli – Chloramine<br>
Kirmeyer, 2004<br>
proportional based on 2-log at CT=64 (15 degC) & CT=40 (20 deg.C)<br>
demonstrated 2.0-log inactivation<br>
level of inactivation calculated by model is capped at 4-log<br>
Although the equation for T>25 is included here for completeness, it is not used in the model

E.Coli – Ozone<br>
Hunt and Marinas, 1997<br>
multiple data points, fit to linear relationship wrt temperature<br>
demonstrated 5.0-log inactivation<br>
level of inactivation calculated by model is capped at 8-log

E.Coli – Chlorine Dioxide<br>
LeChevalier, 2004<br>
based on 2-log inactivation for CT=0.38 (15 deg.C) and CT=0.18 (20 deg.C)<br>
demonstrated 2.0-log inactivation<br>
level of inactivation calculated by model is capped at 4-log

E.Coli – Ultraviolet<br>
Hijnen, 2006<br>
k-value of 0.642 reduced by factor of 3 for observed environmenal strain resistance<br>
5.5-log inactivation observed in cited studies (Wilson 1992)<br>
level of inactivation calculated by model is capped at 5.5-log

### Pathogen concentration in treated water

- for each 'test' concentration in raw water, the treated water is calculated by applying the overall log reduction through treatment<br>
  Equation: Conc(treated) = Conc(raw) x 10-(log reduction)

### Daily pathogen dose ingested

- mean dose of pathogens ingested (#/day) = pathogen concentration (# / L) x daily water consumption (L/day)
- the model default is 1 liter of tap water consumed, unboiled (rounded)

Age Group | Sample Size | Average Cold Water Consumpation (L/day)
--------- | ----------- | ---------------------------------------
<1        | 283         | 0.05
1–4       | 2872        | 0.25
5–11      | 4917        | 0.38
12–19     | 6729        | 0.59
20–59     | 11 757      | 0.64
60+       | 6911        | 0.51
All ages  | 33 469      | 0.57

Statistics Canada (2004). Data Source: Canadian Community Health Survey, Cycle 2.2 – Nutrition (Wave 3): General Health and 24-hour Dietary recall (2004). (Share File)<br>
Statistics Canada (2008). User Guide: Canadian Community Health Survey (CCHS), Cycle 2.2 (2004), Nutrition - General Health (including Vitamin & Mineral Supplements) & 24-hour Dietary recall components. Ottawa.

Probability of infection (daily) using Dose-response equations

- dose response equations based on literature reported models, usually based on human experimental data or from disease outbreaks
- dose-response equations are summarized below along with literature reference Model Equation
- the exponential and beta-Poisson models are used to calculate daily risk of infection for a given dose of pathogens ingested (1, 2, 3...etc.) Binomial
- the exponential model is the product of a binomial distribution and poisson distribution
- the beta-Poisson model is the product of a beta-binomial distribution and poisson distribution

Pathogen      | r       | α      | β      | Model        | Reference
------------- | ------- | ------ | ------ | ------------ | -------------------
Crypto        | 0.018   |        |        | Exponential  | Messner et al, 2001
Giardia       | 0.01982 |        |        | Exponential  | Rose & Gerba, 1991
Rotavirus     |         | 0.265  | 0.4415 | Beta-poisson | Haas, 1999
Campylobacter |         | 0.145  | 7.59   | Beta-poisson | Medema et al., 1996
E.coli_O157   |         | 0.0571 | 2.2183 | Beta-poisson | Strachan, 2005

r, α, β = coefficients<br>
davg = mean dose of pathogens ingested (#/day)<br>
j / n = exact number of organisms ingested (eg. 1,2,3...100)<br>
B = beta function

### Probability of illness (daily), given infection

- defines the probability of illness given infection since not all infections lead to illness (symptoms)
- values from literature, usually based on illness rates observed during disease outbreaks

Pathogen      | Probability of Illness, given infection | Reference
------------- | --------------------------------------- | -----------------------------------
Crypto        | 0.7                                     | Casman et al, 2000
Giardia       | 0.4                                     | Nash et al. 1987
Rotavirus     | 0.88                                    | Havelaar & Melse, 2003
Campylobacter | 1.0                                     | Illness used in dose response model
E.coli_O157   | 1.0                                     | Illness used in dose response model

### Probability of illness (annual)

- to calculated the probability of 1 or more illnesses per year, given a daily risk of illness P(ill,daily), following equation can be used:<br>
  Equation (1): P(ill, yr) = 1 - [1 - P(ill, daily)]N

- daily risk of illness can also be multiplied by 365 days/year to estimate annual risk of illness for daily exposures<br>
  Equation (2): P(ill, yr) = P(ill, daily) x 365 days/yr

- NOTE: at very low levels of risk, the two above equations give the same result; due to limitations in excel, daily risk of illness <10-6 use equation(2)
- neither equation accounts for any acquired immunity in population, and allows multiple illnesses to re-occur at high dose exposures

Calculation of total illnesses in population

- annual risk of illness (individual) multiplied by population = total # of illnesses per year<br>
  Equation: P(ill, yr) x Population = # illnesses per year

DALY calculations to estimate burden of disease per case of illness

- DALY is a health metric used by World Health Organization (2004)
- DALYs = Disability Adjusted Life Years
- considers the severity and duration of illness for various health outcomes (eg. diarrhea, kidney disease, death, etc.)

DALY = LYL + YLD where: LYL = life years lost (due to premature death)<br>
YLD = years lived with a disability or impairment to health

LYL = [ life expectancy - age at death ] x W W = severity weight factor for death = 1.0

YLD = L x W L = duration of illness (in years)<br>
W = severity weight factor for illness or impairment [ranges from 0 to 1]

- population age fractions are used to determine life expectancy in LYL calculation
- current version of model assumes the same disease rates for each of the population age fractions
- population fractions are based on the following reference values:


  Fraction of population    Fraction of population

  |Age Category| Median Age| (Males) | (Females)|
	|---|---|---|---|
  |(0-14 years) | 7 | 0.17 | 0.16
  |(15 – 64 year) | 39.5 | 0.69 | 0.68
  |(65 +) yrs* | 72.94| 0.13 | 0.16
  |total population | | 16414230 | 17062460

- Based on the 2011 census data for Canada(Statistics Canada, 2013)
- note for this age category, 80.88 years is used as the upper range for calculations (i.e. average life expectancy for Canadian population)
- the weighted average age in population = 38.98

|Category |Life Expectancy at birth (years)
|--|--|
|Males | 78.64 |
|Females | 83.12 |
|Combined | 80.88 |
|Difference | 4.48 |

- Stats Canada (2012)
- *LYL = [ life expectancy - age at death ] x W = 41.90*

### Health outcome fractions for each reference pathogen
Illness Outcome|Cryptoc|Giardiaa|Rotavirusa|Campyl.b|E.coli\_O157
-----|-----|-----|-----|-----|-----
mild diarrhea|0.99999|0.99999|0.50|1.00|0.53
bloody diarrhea| | |0.50|0.06|0.47
GBS, clinical & residual| | | |0.0002|
GBS, residual| | | |0.0002|
Reactive arthritis| | | |0.023|
HUS| | | | |0.01
ESRD| | | | |0.00118
death (GBS)| | | |0.0000046 (1 in 217,000)|
death|0.00001 (1 in 100,000)|0.00001 (1 in 100,000)|0.0001 (1 in 10,000)|0.0001 1 in 10,000)|0.00025 )1 in 4,000)

- Macler & Regli (1993); bHavelaar & Melse (2003); cassumed to be same as Giardia

Duration of illness (years) for various health outcomes (Havelaar & Melse 2003)<br>

Illness Outcome | Crypto | Giardia | Rotavirus | Campyl. | E.coli_O157
-----------------|--------|---------|-----------|---------|-----------
mild diarrhea | 0.01918 (7 days) | 0.01918 (7 days)| 0.01918 (7 days) | 0.01397 (5.1 days) | 0.00932 (3.4 days)
serious diarrhea (ie. bloody) | | | 0.01918 (7 days)| 0.0230 (8.4 days) | 0.01534 (5.6 days)
HUS | | | | |0.0575 (21 days)
ESRD | | | | | 9.35
clinical (GBS) | | | |  Wxd=0.29< |
residual (GBS) | | | | Wxd=5.8 |
reactive arthritis (GBS) | | | |  0.115 (6 weeks)|
death (GBS) | | | | (e _- 59.7) |
death |(e_ - adeath) |  (e _- adeath) | (e_ - adeath) | (e _- 65.2) | (e_ - adeath)

- e* = life expectancy; adeath = age at death from illness<br>
- primarily affects elderly population (yrs) = 67.68 62.18

Severity weights for each illness are taken from reference tables (World Health Organization).

- Global Burden of Disease & Injury Series - Vol.II; Murray & Lopez 1996a, 1996b

Illness Outcome|Severity
-----|-----
mild diarrhea|0.067
serious/bloody diarrhea|0.39
HUS|0.93
ESRDd|0.95
reactive arthritis |0.21
clinical effects (GBS)|Wxd=0.29
residual effects (GBS)|Wxd=5.8
death|1.0

Sum of health outcome fractions (f), severity (W), & duration (L) to determine DALY impact per case of illness<br>
Equation: DALY = ∑ fi Li Wi + fj Lj Wj +fk Lk Wk +...

Summary of DALY's per case of illness for all (5) microbial pathogens (2011 census info)<br>

|Crypto|Giardia|Rotavirus|Campyl.|E.coliO157
-----|-----|-----|-----|-----|-----
DALY’s due to morbidity (YLD)|1.29E-03|1.29E-03|4.38E-03|3.25E-03|1.42E-02
DALY’s due to mortality (LYL)|3.90E-04|3.90E-04|3.90E-03|1.41E-03|9.75E-03
TOTAL DALY’s|1.67E-03|1.67E-03|8.28E-03|4.65E-03|2.39E-02

Calculation of total DALYs for population

- DALYs for population are calculated by multiplying #cases illness times the DALYs per case for each specific pathogen
 - Total DALY impact on population calculated by adding DALYs for each pathogen:
- DALYs (population) = [Annual # cases illness] x DALY/case
- Total DALYs (population) = DALY(Crypto) + DALY(Giardia) + DALY(Rotavirus) + DALY(Campy) + DALY(Ecoli)
