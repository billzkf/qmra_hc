//list of functions for free chlorine applied to various pathogens
var freeChlorine = {};

/* Crypto – Free Chlorine
 * Korich 1990
 * 2-log demonstrated @ pH 7, 25°C; CT = 7200 mg/L-min
 * level of inactivation calculated by model is capped at 4-log
 */
freeChlorine['crypto'] = function(time, residual) {
  var result = time * residual * 2 / 7200;
  return result;
}

/* Giardia – Free Chlorine
 * curve fit to data from EPA SWTR (1999)
 * no safety factors applied to CT tables, but used 99% UCL statistical fit of experimental data
 * 4-log inactivation demonstrated at pH=6-9
 * level of inactivation calculated by model is capped at 8-log
 */
freeChlorine['giardia'] = function(time, residual, temp, pH) {
  var result = time * Math.pow(residual, 0.85) / (0.2828 * Math.pow(pH, 2.69) * Math.pow(0.933, (temp - 5)));
  return result;
}

/* Rotavirus - Free Chlorine
 * curve fit to original data from Sobsey, 1988 (Hepatitis A virus); data cited in EPA SWTR (1999)
 * NOTE: For information, US-EPA CT tables included a safety factor of 3
 * NOTE: CT values based on HAV although Rotavirus may be 20-25 times more easily inactivated
 * 4-log inactivation demonstrated at 5ºC, pH 6-9
 * 8 level of inactivation calculated by model is capped at 8-log
 */
freeChlorine['rota'] = function(time, residual, temp, pH) {
  var result = time * residual * 0.692 * Math.exp(0.0713 * temp) / (-0.0667 * Math.pow(pH, 3) + 1.59 * Math.pow(pH, 2) - 12.43 * pH + 32.34);
  return result;
}

/* Campylobacter – Free Chlorine
 * Blaser,1986
 * average values across all temperature & pH conditions cited
 * proportional based on 3.64-log at CT=0.5 mg/L-min
 * 3.6-log inactivation demonstrated at various pH and temperature values
 * level of inactivation calculated by model is capped at 8-log
 */
freeChlorine['campy'] = function(time, residual) {
  var result = 3.64 / 0.5 * time * residual;
  return result;
}

/* E.Coli – Free Chlorine
 * Rice and Clark, 1999
 * curve fit for 7 serotypes of EcoliO157
 * demonstrated 4.5-log inactivation
 * level of inactivation calculated by model is capped at 8-log
 */
freeChlorine['eColi'] = function(time, residual) {
  var result = 3.8962 * Math.pow(time * residual, 0.3124);
  return result;
}

var chloramine = {};

/* Finch, G. R., Gyurek, L. L., & Liyanage, L. R. (1997).
* Hom model using k=1.1x10-6  n=2.53  m=1.28
* 2.3-log demonstrated at pH=8, 22ºC;
* level of inactivation calculated by model is capped at 5-log
*/
chloramine['crypto'] = function(time, residual) {
  var result = 0.0000011 * Math.pow(residual, 2.53) * Math.pow(time, 1.28);
  return result;
}

/*curve fit to data from EPA SWTR (1999)
* 2-log inactivation demonstrated at pH=8
* level of inactivation calculated by model is capped at 4-log
*/
chloramine['giardia'] = function(time, residual, temp) {
  var result = time * residual * (0.0000068 * Math.pow(temp, 2) - 0.0000764 * temp + 0.001619);

  return result;
}
/* curve fit to data from EPA SWTR (1999)
* NOTE: CT values were based on HAV;  Rotavirus may be 5-times more resistant than HAV
* 2-log inactivation demonstrated at 5ºC, pH=7
* level of inactivation calculated by model is capped at 4-log
*/
chloramine['rota'] = function(time, residual, temp) {
  var result = 0.2439 * Math.pow(time * residual / 100, 0.82) * Math.exp(0.0588 * temp);
  return result;
}

/*
* Blaser,1986
* used average values across all temperature & pH conditions cited
* proportional based on 3.77-log at CT=15 mg/L-min
* 3.8-log inactivation demonstrated at various pH and temperature values
* level of inactivation calculated by model is capped at 8-log
*/
chloramine['campy'] = function(time, residual) {
  var result = 3.77 / 15 * time * residual
  return result;
}
/*
Kirmeyer, 2004
proportional based on 2-log at CT=64 (15 degC) & CT=40 (20 deg.C)
demonstrated 2.0-log inactivation
level of inactivation calculated by model is capped at 4-log
Although the equation for T>25 is included here for completeness, it is not used in the model
*/
chloramine['eColi'] = function(time, residual) {
  result = (2 / 64) * time * residual;
  return result;
}

var ozone = {}

/*
* EPA (2006) less conservative model than previous EPA guidance documents
* EPA applied 90% confidence boundary to the data set
* no sufficient evidence for inactivation beyond 3-log, although Finch(1996) observed 4.7-log with CT=36 at pH=6.9 and 22ºC
* level of inactivation calculated by model is capped at 6-log
*/
ozone['crypto'] = function(time, residual, temp) {
  var ke = Math.exp(25.2373398441227) * Math.exp(-62818.7684115104 / (8.314 * (273.15 + temp)));
  var result = -Math.log10(Math.exp(-ke * time * residual));
  return result;
}

/*curve fit to data from EPA SWTR (1999)
* safety factor of 2 in EPA table - removed for model calc's
* 2-log inactivation demonstrated at 5ºC, pH 7
* level of inactivation calculated by model is capped at 4-log
*/
ozone['giardia'] = function(time, residual, temp) {
  var result = time * residual * (0.0087 * Math.pow(temp, 2) + 0.0334 * temp + 1.545);
  return result;
}

/* curve fit to data from EPA SWTR (1999)
* safety factor of 3 in EPA table - removed for model calc's
* NOTE: CT values based on Poliovirus1, but Rotavirus may be 3-30 times more easily inactivated
* 2-log inactivation demonstrated at 5ºC, pH 7
* level of inactivation calculated by model is capped at 4-log
*/
ozone['rota'] = function(time, residual, temp) {
  var result = time * residual * Math.exp(0.7423 * Math.exp(0.07 * temp));
  return result;
}

/*
* use CT values for E.Coli (no Campylobacter studies available)
* demonstrated 5.0-log inactivation
* level of inactivation calculated by model is capped at 8-log
*/
ozone['campy'] = function(time, residual) {
  var result = -Math.log10(Math.exp(-499 * time * residual));
  return result;
}

/* Hunt and Marinas, 1997
* multiple data points, fit to linear relationship wrt temperature
* demonstrated 5.0-log inactivation
* level of inactivation calculated by model is capped at 8-log
*/
ozone['eColi'] = function(time, residual) {
  var result = -Math.log10(Math.exp(-499 * time * residual))
  return result;
}


var chlorineDioxide = {}

/*
* EPA (2006)
* less conservative model than previous EPA guidance documents
* EPA applied 90% confidence boundary to the data set
* 3.0-log inactivation demonstrated at pH=8 and 22ºC
* level of inactivation calculated by model is capped at 6-log
*/
chlorineDioxide['crypto'] = function(time, residual, temp) {
  var result = 0.001506 * time * residual * Math.pow(1.09116, temp);
  return result;
}

/*Curve fit to data from EPA SWTR (1999)
* safety factor of 1.5 in EPA table - removed for model calc's
* 2-log inactivation demonstrated
* level of inactivation calculated by model is capped at 4-log
*/
chlorineDioxide['giardia'] = function(time, residual, temp) {
  var result = time * residual * (0.0003949 * Math.pow(temp, -0.0041565) * temp + 0.12843);
  return result;
}

/*
curve fit to data from EPA SWTR (1999)
safety factor of 2 in EPA table - removed for model calc's
NOTE: CT values based on HAV though Rotavirus may be 20-times more easily inactivated
4-log inactivation demonstrated at 5ºC, pH 7
level of inactivation calculated by model is capped at 8-log
*/
chlorineDioxide['rota'] = function(time, residual, temp) {
  var result = Math.pow(time * residual, 0.3854) * (0.03507 * temp + 0.82447);
  return result;
}

/*use CT values for E.Coli (no Campylobacter studies available)
* demonstrated 2.0-log inactivation
* level of inactivation calculated by model is capped at 4-log
*/
chlorineDioxide['campy'] = function(time, residual, temp) {
  var result;
  if (temp < 20) {
    result = 2 * time * residual / 0.38;
  } else {
    result = 2 * time * residual / 0.18;
  }
  return result;
}

/*
* LeChevalier, 2004
* based on 2-log inactivation for CT=0.38 (15 deg.C) and CT=0.18 (20 deg.C)
* demonstrated 2.0-log inactivation
* level of inactivation calculated by model is capped at 4-log
*/
chlorineDioxide['eColi'] = function(time, residual, temp) {
  var result;
  if (temp < 20) {
    result = 2 * time * residual / 0.38
  } else {
    result = 2 * time * residual / 0.18
  }
  return result;
}

var BaffleNCSTR = function(baffle) {
  var N = 2280.1 * Math.pow(baffle, 6) - 4179 * Math.pow(baffle, 5) + 2890.6 * Math.pow(baffle, 4) - 862.24 * Math.pow(baffle, 3) + 98.756 * Math.pow(baffle, 2) + 5.6287 * baffle;
  return N;
}

var ReactorChar = function(hrt, baffle, temp, pH, kValue, initConc, slices) {
  slices = slices || 1000;
  var N = BaffleNCSTR(baffle);
  let seq = jStat.seq(0, slices - 1, slices);
  var reactorChar = {
    xValue: seq,
    CDF: seq.map(function(d) {
      return 0;
    }),
    PDF: seq.map(function(d) {
      return 0;
    }),
    time: seq.map(function(d) {
      return 0;
    }),
    residual: seq.map(function(d) {
      return 0;
    }),
    IntCt: seq.map(function(d) {
      return 0;
    })
  };

  for (var i = 1; i < slices; i++) {
    var prev = {
      xValue: reactorChar.xValue[i - 1],
      CDF: reactorChar.CDF[i - 1],
      PDF: reactorChar.PDF[i - 1],
      time: reactorChar.time[i - 1],
      residual: reactorChar.residual[i - 1],
      IntCt: reactorChar.IntCt[i - 1]
    };
    reactorChar.xValue[i] = prev.xValue + jStat.gamma.inv(0.999, N, 1) / 1000;
    reactorChar.CDF[i] = jStat.gamma.cdf(reactorChar.xValue[i], N, 1);
    reactorChar.PDF[i] = reactorChar.CDF[i] - prev.CDF;
    reactorChar.time[i] = reactorChar.xValue[i] / N * hrt;
    reactorChar.residual[i] = Math.exp(-kValue * reactorChar.time[i]) * initConc;
    reactorChar.IntCt[i] = (initConc * -Math.exp(-kValue * reactorChar.time[i]) / kValue) - (initConc * -Math.exp(-kValue * reactorChar.time[1]) / kValue);
  }
  return reactorChar;
}

var LogInactivation = function(reactChar, treatment, temp, pH) {
  var slices = reactChar.xValue.length;

  //intv results
  var intvResults = {
    crypto: [0],
    giardia: [0],
    rota: [0],
    campy: [0],
    eColi: [0]
  };
  //cumm results
  var cummResults = {
    crypto: [0],
    giardia: [0],
    rota: [0],
    campy: [0],
    eColi: [0]
  };


  for (var i = 1; i < slices; i++) {
    var timeIntv = reactChar.time[i] - reactChar.time[i - 1];
    switch (treatment) {
      case 'freeChlorine':
        intvResults.crypto[i] = freeChlorine["crypto"](timeIntv, reactChar.residual[i]);
        intvResults.giardia[i] = freeChlorine['giardia'](timeIntv, reactChar.residual[i], temp, pH);
        intvResults.rota[i] = freeChlorine["rota"](timeIntv, reactChar.residual[i], temp, pH);
        intvResults.campy[i] = freeChlorine["campy"](timeIntv, reactChar.residual[i]);
        intvResults.eColi[i] = freeChlorine["eColi"](timeIntv, reactChar.residual[i]);
        break;
      case 'chloramine':
        intvResults.crypto[i] = chloramine["crypto"](timeIntv, reactChar.residual[i]);
        intvResults.giardia[i] = chloramine["giardia"](timeIntv, reactChar.residual[i], temp);
        intvResults.rota[i] = chloramine["rota"](timeIntv, reactChar.residual[i], temp);
        intvResults.campy[i] = chloramine["campy"](timeIntv, reactChar.residual[i]);
        intvResults.eColi[i] = chloramine["eColi"](timeIntv, reactChar.residual[i]);
        break;
      case 'ozone':
        intvResults.crypto[i] = ozone["crypto"](timeIntv, reactChar.residual[i], temp);
        intvResults.giardia[i] = ozone["giardia"](timeIntv, reactChar.residual[i], temp);
        intvResults.rota[i] = ozone["rota"](timeIntv, reactChar.residual[i], temp);
        intvResults.campy[i] = ozone["campy"](timeIntv, reactChar.residual[i]);
        intvResults.eColi[i] = ozone["eColi"](timeIntv, reactChar.residual[i]);
        break;
      case 'chlorineDioxide':
        intvResults.crypto[i] = chlorineDioxide["crypto"](timeIntv, reactChar.residual[i], temp);
        intvResults.giardia[i] = chlorineDioxide["giardia"](timeIntv, reactChar.residual[i], temp);
        intvResults.rota[i] = chlorineDioxide["rota"](timeIntv, reactChar.residual[i], temp);
        intvResults.campy[i] = chlorineDioxide["campy"](timeIntv, reactChar.residual[i], temp);
        intvResults.eColi[i] = chlorineDioxide["eColi"](timeIntv, reactChar.residual[i], temp);
        break;
    }
    cummResults.crypto[i] = intvResults.crypto[i] + cummResults.crypto[i - 1];
    cummResults.giardia[i] = intvResults.giardia[i] + cummResults.giardia[i - 1];
    cummResults.rota[i] = intvResults.rota[i] + cummResults.rota[i - 1];
    cummResults.campy[i] = intvResults.campy[i] + cummResults.campy[i - 1];
    cummResults.eColi[i] = intvResults.eColi[i] + cummResults.eColi[i - 1];
  }
  return [intvResults, cummResults];
};

var RemainingOrganisms = function(reactChar, logInact, treatment) {
  var offset = 1E50;
  var slices = reactChar.xValue.length;
  var organisms = ["crypto", "giardia", "rota", "campy", "eColi"];
  var remaining = {
    crypto: [],
    giardia: [],
    rota: [],
    campy: [],
    eColi: []
  };
  for (j = 0; j < organisms.length; j++) {
    var org = organisms[j];
    for (var i = 0; i < slices; i++) {
      remaining[org][i] = reactChar.PDF[i] * offset * Math.pow(10, -logInact[org][i]);
    }
  }
  return remaining;
}

var checkMax = function(type, org, value){
  var maxValues = {
    freeChlorine:{
      crypto:4,
      giardia:8,
      rota:8,
      campy:8,
      eColi:8
    },
    chloramine:{
      crypto:5,
      giardia:4,
      rota:4,
      campy:8,
      eColi:4
    },
    ozone:{
      crypto:6,
      giardia:4,
      rota:4,
      campy:8,
      eColi:8
    },
    chlorineDioxide:{
      crypto:6,
      giardia:4,
      rota:8,
      campy:4,
      eColi:4
    }
  };
  return Math.min(value, maxValues[type][org]);
}

var getReducedConc = function(type, hrt, BF, temp, initConc, kValue, pH) {
  var red = {
    crypto: 0,
    giardia: 0,
    rota: 0,
    campy: 0,
    eColi: 0
  };
  if (type === "none" || type === "userSpecified") {
    return {
      reduction: red,
      reactChar: null
    };
  } else {
    var rc = ReactorChar(hrt, BF, temp, pH, kValue, initConc);
    var li = LogInactivation(rc, type, temp, pH);
    var ro = RemainingOrganisms(rc, li[1]);
    for(var org in ro){
      var value = -Math.log10(jStat.sum(ro[org]) / 1E50);
      red[org] = checkMax(type, org, value);
    }
    return {
      reduction: red,
      reactChar: rc
    };
  }
}



onmessage = function(event) {

  importScripts('./res/jStat/dist/jstat.min.js', './utils.js');
  var dis = event.data[0];
  var result = getReducedConc(dis.short, dis.contactTime, dis.baffle, dis.temp, dis.initConc, dis.kValue, dis.pH);
  postMessage(result);
  close();
}
